package com.example.jaafweb.exception;


import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionController {

    private static final Logger LOG=LoggerFactory.getLogger(ExceptionController.class.getName());

    @ExceptionHandler({UserNotFoundException.class})
    public ProblemDetail UserNotExistException(UserNotFoundException ex){
        LOG.error(ex.getMessage(),ex);
        ProblemDetail  problemDetail=ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, ex.getMessage());
        problemDetail.setProperty("User with Id",ex.getId());
        return problemDetail;
    }

    @ExceptionHandler({UserAlreadyExistException.class})
    public ProblemDetail UserExistException(UserAlreadyExistException ex){
        LOG.error(ex.getMessage(),ex);
        return ProblemDetail.forStatusAndDetail(HttpStatus.CONFLICT, ex.getMessage());

    }
 
}
