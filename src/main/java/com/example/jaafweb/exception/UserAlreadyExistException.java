package com.example.jaafweb.exception;

public class UserAlreadyExistException extends RuntimeException{


    private Integer id;
    public UserAlreadyExistException() {
    }

    public UserAlreadyExistException(String message, Integer id) {
        super(message);
        this.id=id;
    }

    public UserAlreadyExistException(String message, Throwable cause, Integer id) {
        super(message, cause);
        this.id=id;
    }

    public Integer getId() {
        return id;
    }


}
