package com.example.jaafweb.exception;


public class UserNotFoundException extends RuntimeException{



    private Integer id;
         public UserNotFoundException(Integer userId){
            super(String.format("The user with id [%d] is not in the system, try entering the id again", userId));
            this.id=userId;
         }

    public UserNotFoundException(String message,  Integer id) {
        super(message);
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
