package com.example.jaafweb.controller;


import com.example.jaafweb.entity.AcademicEntity;
import com.example.jaafweb.entity.ProjectEntity;
import com.example.jaafweb.entity.UserEntity;
import com.example.jaafweb.entity.WorkEntity;
import com.example.jaafweb.exception.UserNotFoundException;
import com.example.jaafweb.service.AcademicService;
import com.example.jaafweb.service.ProjectService;
import com.example.jaafweb.service.UserService;
import com.example.jaafweb.service.WorkService;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Log4j2
@RestController
public class JaafWebController {

    @Autowired
    private UserService userService;

    @Autowired
    private AcademicService academicService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private WorkService workService;

    public JaafWebController(UserService userService)  {
        this.userService = userService;
    }


    //Crud endpoints

    @PostMapping("/api/user/")
    public ResponseEntity<?> create (@RequestBody UserEntity user){
        //log.trace("User create");

        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(user));
    }

    @GetMapping("/api/user/{id}")
    public ResponseEntity<?> read(@PathVariable(value="id") Integer userId){// throws UserNotFoundException {
       //log.trace("User read ");
       Optional<UserEntity> oUser=userService.findUserById(userId);
       if(!oUser.isPresent())
        {
           return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(oUser);
    }

    @PutMapping("/api/cpanel/user/update/")
    public ResponseEntity<?> update(@RequestBody UserEntity user) throws RuntimeException{
        //log.trace("User update");
        UserEntity tUser;
        if(user==null)
        {
            throw new NullPointerException("Trying to search for a user but the id is null.");
        }
    try{
        tUser=userService.updateUser(user);
        }catch (EntityNotFoundException ex)
        {

            throw  new UserNotFoundException("The user is not correct or there are errors in the data",user.getId());
        }

        return  ResponseEntity.ok().build();
    }

    @DeleteMapping("/api/cpanel/user/{id}")
    public ResponseEntity<?> delete (@PathVariable(value="id") Integer id) throws UserNotFoundException{
        //log.trace("User read from cpanel");

        if(!userService.findUserById(id).isPresent())
        {
           throw new UserNotFoundException(id);
        }
        userService.deleteUser(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/")
    public List<UserEntity> readAll()
    {
        //log.trace("Read all user");
        List<UserEntity> usersList= StreamSupport.stream(userService.findAllUsers().spliterator(),false).collect(Collectors.toList());

        return usersList;
    }

    //Academic
    @GetMapping("/api/academic/{academicId}")
    public ResponseEntity<?> findAcademicById(@PathVariable(value="academicId") Integer acadedmicId )
    {
        //log.trace("Find academic by id");
        Optional<AcademicEntity> oAcademic=academicService.findAcademicById(acadedmicId);
        if(!oAcademic.isPresent())
        {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(oAcademic);
    }

    @GetMapping("/api/academic/")
    public List<AcademicEntity> findAllAcademic()
    {
        //log.trace("Find all academic ");
        List<AcademicEntity> academicList=StreamSupport.stream(academicService.findAllAcademic().spliterator(),false).collect(Collectors.toList());
        return academicList;
    }




    //Project
    @GetMapping("/api/project/")
    public List<ProjectEntity> findAllProjects()
    {
        //log.trace("Find all projects");
        List<ProjectEntity>  projectList=StreamSupport.stream(projectService.findAllProjects().spliterator(), false).collect(Collectors.toList());
        return projectList;
    }

    @GetMapping("/api/project/{projectId}")
    public ResponseEntity<?> findProjectById(@PathVariable(value="projectId") Integer projectId)
    {
        //log.trace("Find project by Id");
        Optional<ProjectEntity> oProject=projectService.findProjectById(projectId);
        if(!oProject.isPresent())
        {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(oProject);
    }

    //Work
    @GetMapping("api/work/")
    public List<WorkEntity> findAllWorks()
    {
        //log.trace("Find all works");
        List<WorkEntity> workList=StreamSupport.stream(workService.findAllWork().spliterator(), false).collect(Collectors.toList());
        return workList;
    }

    @GetMapping("/api/work/{workId}")
    public ResponseEntity<?> finWorkById(@PathVariable(value="workId") Integer workId)
    {
        //log.trace("Find all works");
        Optional<WorkEntity> oWork=workService.findWorkById(workId);
        if(!oWork.isPresent())
        {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(oWork);

    }


    //End Points
    @GetMapping("/info")
    public String Info() {return "Información personal";}

    @GetMapping("/Project")
    public String project() {return "Proyectos realizados";}

    @GetMapping("/contact")
    public String contact(){return "Contacto";}






}
