package com.example.jaafweb.entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.Email;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="user_entity")
public class UserEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name= "name", nullable = false, length = 20)
    private String name;

    @Column(name="surname", length=40)
    private String surname;

    @Column(name="idenDocument", nullable = false, length = 15)
    private String idenDocument;

    @Email
    @Column(name="userEmail", nullable = false)
    private String userEmail;


    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)  //Fetch=FetchType.LAZY <-Consulta unicamente cuando es necesario
    private List<AcademicEntity> academicList;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<ProjectEntity> projectList;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<WorkEntity> workList;


    public UserEntity() {
    }


    public UserEntity (Builder builder){
        this.name=builder.name;
        this.idenDocument=builder.idenDocument;
        this.userEmail=builder.userEmail;
        this.surname=builder.surname;

    }

    public void newAcademicTitle(AcademicEntity academicObject){
        if(academicList==null) academicList=new ArrayList<>();

        academicObject.setUser(this);
        academicList.add(academicObject);
    }
    public void newProject(ProjectEntity projectObject){
        if(projectList==null) projectList=new ArrayList<>();

        projectObject.setUser(this);
        projectList.add(projectObject);
    }

    public void newWork(WorkEntity workObject){
        if(workList==null) workList=new ArrayList<>();

        workObject.setUser(this);
        workList.add(workObject);
    }


    //User Getters and Setters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setIdenDocument(String idenDocument) {this.idenDocument=idenDocument;}

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    //Academic
    public List<AcademicEntity> getAcademicList() {
        return academicList;
    }

    public void deleteAcademic(Integer id){
        this.academicList.remove(id);
    }


    //Project
    public List<ProjectEntity> getProjectList(){
        return projectList;
    }

    public void deleteProject(Integer id){
        this.projectList.remove(id);
    }


    //Work
    public List<WorkEntity> getWorkList() {
        return workList;
    }

    public void deleteWork(Integer workId){
        this.workList.remove(workId);
    }


    // Build Pattern, for simplicity you can use @Build in the class declaration
    public static class Builder{

        private String name;

        private String surname;

        private String idenDocument;

        private String userEmail;

        public UserEntity.Builder name(String name){
            this.name=name;
            return this;
        }
        public UserEntity.Builder surname(String surname){
            this.surname=surname;
            return this;
        }

        public UserEntity.Builder idenDocument(String idenDocument){
            this.idenDocument=idenDocument;
            return this;
        }

        public UserEntity.Builder userEmail(String userEmail){
            this.userEmail=userEmail;
            return this;
        }

        public UserEntity build(){

            return new UserEntity(this);
        }
    }


}
