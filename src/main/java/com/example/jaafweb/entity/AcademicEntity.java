package com.example.jaafweb.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name="academic")
public class AcademicEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer academicId;

    @Column(name= "title_name", nullable = false)
    private String titleName;

    @Column(name="start_date")
    @DateTimeFormat(pattern = "dd/mm/yyyy")
    private LocalDate studyStartDate;

    @Column(name="end_date")
    @DateTimeFormat(pattern = "dd/mm/yyyy")
    private LocalDate studyEndDate;


    @Column (name="description")
    private String qualificationDescription;

    @ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name="user_id")
    @JsonIgnore //Hace que ignore la referencia infinita entre padre e hijo
    private UserEntity user;

    public AcademicEntity() {
    }


    public AcademicEntity (Builder build){
        this.titleName=build.titleName;
        this.studyStartDate=build.studyStartDate;
        this.studyEndDate=build.studyEndDate;
        this.qualificationDescription= build.qualificationDescription;
    }

    //Getters and Setters
    public Integer getId() {

        return academicId;
    }


    public String getName() {

        return titleName;
    }

    public void setName(String name) {

        this.titleName = name;
    }

    public LocalDate getStartDate() {

        return studyStartDate;
    }

    public void setStartDate(LocalDate startDate) {

        this.studyStartDate = startDate;
    }

    public LocalDate getEndDate() {

        return studyEndDate;
    }

    public void setEndDate(LocalDate endDate) {

        this.studyEndDate = endDate;
    }

    public String getDescription() {

        return qualificationDescription;
    }
    public void setDescription(String description) {

        this.qualificationDescription = description;
    }

    public UserEntity getUser() {

        return user;
    }

    public void setUser(UserEntity user) {

        this.user = user;
    }

    //Build Pattern, for simplicity you can use @Build in the class declaration
    public static class Builder{
        private String titleName;
        private LocalDate studyStartDate;
        private LocalDate studyEndDate;
        private String qualificationDescription;


        public AcademicEntity.Builder titleName(String name){
            this.titleName=name;
            return this;
        }

        public AcademicEntity.Builder studyStartDate(LocalDate startDate){
            this.studyStartDate=startDate;
            return this;
        }

        public AcademicEntity.Builder studyEndDate(LocalDate endDate){
            this.studyEndDate=endDate;
            return this;
        }

        public AcademicEntity.Builder qualificationDescription(String description){
            this.qualificationDescription=description;
            return this;
        }

        public AcademicEntity build(){
            return new AcademicEntity(this);
        }


    }
}
