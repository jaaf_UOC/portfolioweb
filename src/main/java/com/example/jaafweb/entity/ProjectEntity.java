package com.example.jaafweb.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name="project_entity")
public class ProjectEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer projectId;

    @Column(name="projectName", nullable = false, length = 100)
    private String projectName;

    @Column(name="projectDescription", length = 1000)
    private String projectDescription;


    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="user_id")
    @JsonIgnore
    private UserEntity user;

    public ProjectEntity() {
    }

    public ProjectEntity (Builder builder){

        this.projectName=builder.projectName;
        this.projectDescription=builder.projectDescription;

    }

    //Getter and Setter
    public Integer getId() {

        return projectId;
    }

    public String getProjectName() {

        return projectName;
    }

    public void setProjectName(String projectName) {

        this.projectName = projectName;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }



    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user){
        this.user=user;
    }




    //Build Pattern, for simplicity you can use @Build in the class declaration
    public static class Builder{
        private String projectName;

        private String projectDescription;


    public ProjectEntity.Builder projectName(String projectName){
        this.projectName=projectName;
        return this;
    }

    public ProjectEntity.Builder projectDescription(String projectDescription){
        this.projectDescription=projectDescription;
        return this;
    }

    public ProjectEntity build(){

        return new ProjectEntity(this);
    }

    }
}
