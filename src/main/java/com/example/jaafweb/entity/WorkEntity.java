package com.example.jaafweb.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table (name="work_Experience")
public class WorkEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer workId;

    @Column(name="company_name")
    private String companyName;

    @Column(name="job", nullable = false)
    private String job;

    @Column(name="startJobDate")
    @DateTimeFormat(pattern="dd/mm/yyyy")
    private LocalDate startJobDate;

    @Column(name="endJobDate")
    @DateTimeFormat(pattern = "dd/mm/yyyy")
    private LocalDate endJobDate;

    @Column(name="jobDescription")
    private String jobDescription;


    //Relationships between entities
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="user_Id")
    @JsonIgnore
    private UserEntity user;


    public WorkEntity() {
    }

    public WorkEntity (Builder builder){
        this.companyName=builder.companyName;
        this.job=builder.job;
        this.startJobDate=builder.startJobDate;
        this.endJobDate=builder.endJobDate;
        this.jobDescription=builder.jobDescription;
    }


    //Getters and Setters
    public Integer getWorkId() {
        return workId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        job = job;
    }

    public LocalDate getStartJobDate() {
        return startJobDate;
    }

    public void setStartJobDate(LocalDate startJobDate) {
        this.startJobDate = startJobDate;
    }

    public LocalDate getEndJobDate() {
        return endJobDate;
    }

    public void setEndJobDate(LocalDate endJobDate) {
        this.endJobDate = endJobDate;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        jobDescription = jobDescription;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }


    //Build Pattern, for simplicity you can use @Build in the class declaration
    public static class Builder{

        private String companyName;
        private String job;
        private LocalDate startJobDate;
        private LocalDate endJobDate;
        private String jobDescription;

        public WorkEntity.Builder companyName(String name){
            this.companyName=name;
            return this;
        }

        public WorkEntity.Builder job(String job){
            this.job=job;
            return this;
        }


        public WorkEntity.Builder startJobDate(LocalDate date){
            this.startJobDate=date;
            return this;
        }


        public WorkEntity.Builder endJobDate (LocalDate date){
            this.endJobDate=date;
            return this;
        }


        public WorkEntity.Builder  jobDescription (String description){
            this.jobDescription=description;
            return this;
        }

        public WorkEntity build(){

            return new WorkEntity(this);
        }

    }
}
