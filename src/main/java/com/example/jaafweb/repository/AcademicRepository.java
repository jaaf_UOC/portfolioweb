package com.example.jaafweb.repository;

import com.example.jaafweb.entity.AcademicEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcademicRepository extends JpaRepository <AcademicEntity, Integer> {



}
