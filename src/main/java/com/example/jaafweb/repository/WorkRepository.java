package com.example.jaafweb.repository;

import com.example.jaafweb.entity.WorkEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkRepository extends JpaRepository <WorkEntity, Integer> {
}
