package com.example.jaafweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class JaafWebApplication{

    public static void main(String[] args) {
        SpringApplication.run(JaafWebApplication.class, args);
    }

}
