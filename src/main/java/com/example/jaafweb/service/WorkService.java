package com.example.jaafweb.service;

import com.example.jaafweb.entity.WorkEntity;

import java.util.Optional;

public interface WorkService {

    public Optional<WorkEntity> findWorkById(Integer workId);

    public Iterable<WorkEntity> findAllWork();
}
