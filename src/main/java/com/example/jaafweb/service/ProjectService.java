package com.example.jaafweb.service;

import com.example.jaafweb.entity.ProjectEntity;

import java.util.Optional;

public interface ProjectService {

    public Optional<ProjectEntity> findProjectById(Integer projectId);

    public Iterable<ProjectEntity> findAllProjects();
}
