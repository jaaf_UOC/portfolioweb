package com.example.jaafweb.service;

import com.example.jaafweb.entity.AcademicEntity;
import com.example.jaafweb.entity.ProjectEntity;
import com.example.jaafweb.entity.UserEntity;

import java.util.List;
import java.util.Optional;

public interface UserService {

    //CRUD
    Integer createUser(UserEntity user);

    void deleteUser(Integer id);

    public UserEntity updateUser(UserEntity user);


    //Users search

    public Optional<UserEntity> findUserById(Integer id);

    public Iterable<UserEntity> findAllUsers();

    //Acadmic
    public List<AcademicEntity> findAllAcademic(Integer id);

    //Public AcademicEntity findAcademic(Integer id);



    //Projects
    public List<ProjectEntity> findAllProject(Integer id);
}