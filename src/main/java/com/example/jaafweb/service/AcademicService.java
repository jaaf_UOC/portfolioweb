package com.example.jaafweb.service;

import com.example.jaafweb.entity.AcademicEntity;

import java.util.Optional;

public interface AcademicService {

    public Optional<AcademicEntity> findAcademicById(Integer academicId);

    public Iterable<AcademicEntity> findAllAcademic();

}
