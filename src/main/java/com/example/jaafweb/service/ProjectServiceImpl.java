package com.example.jaafweb.service;

import com.example.jaafweb.entity.AcademicEntity;
import com.example.jaafweb.entity.ProjectEntity;
import com.example.jaafweb.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProjectServiceImpl implements ProjectService{

    @Autowired
    private ProjectRepository JPARepositoryProject;

    @Override
    public Optional<ProjectEntity> findProjectById(Integer projectId){
        return JPARepositoryProject.findById(projectId);
    }

    public Iterable<ProjectEntity> findAllProjects(){
        return JPARepositoryProject.findAll();
    }
}
