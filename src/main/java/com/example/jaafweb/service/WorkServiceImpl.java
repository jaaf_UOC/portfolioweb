package com.example.jaafweb.service;

import com.example.jaafweb.entity.WorkEntity;
import com.example.jaafweb.repository.UserRepository;
import com.example.jaafweb.repository.WorkRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;


@Log4j2
@Service
public class WorkServiceImpl implements WorkService{

    @Autowired
    private WorkRepository jpaRepositoryWork;

    @Override
    public Optional<WorkEntity> findWorkById(Integer workId){

        return jpaRepositoryWork.findById(workId);
    }

    @Override
    public Iterable<WorkEntity> findAllWork(){

        return jpaRepositoryWork.findAll();
    }
}
