package com.example.jaafweb.service;

import com.example.jaafweb.entity.AcademicEntity;
import com.example.jaafweb.repository.AcademicRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Log4j2
@Service
public class AcademicServiceImpl implements AcademicService{

    @Autowired
    private AcademicRepository  JpaRepositoryAcademic;



   @Override
   public Optional<AcademicEntity> findAcademicById(Integer academicId){

        return JpaRepositoryAcademic.findById(academicId);
    }

    @Override
    public Iterable<AcademicEntity> findAllAcademic(){

       return JpaRepositoryAcademic.findAll();
    }



}
