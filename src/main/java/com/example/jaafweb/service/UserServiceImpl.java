package com.example.jaafweb.service;

import com.example.jaafweb.entity.AcademicEntity;
import com.example.jaafweb.entity.ProjectEntity;
import com.example.jaafweb.entity.UserEntity;
import com.example.jaafweb.exception.UserNotFoundException;
import com.example.jaafweb.repository.AcademicRepository;
import com.example.jaafweb.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

@Log4j2
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private  UserRepository jpaRepository;


//CRUD
    @Override
    public Integer createUser(UserEntity user){
        if(user==null){
            throw new NullPointerException("Trying to create a new user but the information is null.");
        }
        UserEntity newUser=jpaRepository.save(user);
        return newUser.getId();
    }


    @Override
    public void deleteUser(Integer id){
        if(id==null){
            throw new NullPointerException("Trying to search for a user but the id is null.");
        }
        UserEntity user=jpaRepository.findById(id).orElseThrow(()->new IllegalArgumentException(format("It is not possible to delete the user with id [%d], it is not in the system.",id)));
        jpaRepository.deleteById(id);
    }


    @Override
    public UserEntity updateUser(@NonNull UserEntity user)
    {
        UserEntity BBDDUser = jpaRepository.getReferenceById(user.getId());
        //Update user and save in BBDD
        BBDDUser.setName(user.getName());
        UserEntity userUpdated=jpaRepository.save(BBDDUser);
        //log.info("User with id {%d}, is been update", user.getId());
        return BBDDUser;
    }

    //Find
    @Override
    public Iterable<UserEntity> findAllUsers() {

        return jpaRepository.findAll();
    }

    @Override
    public Optional<UserEntity> findUserById(Integer id){
        Optional<UserEntity> oUser = Optional.ofNullable(jpaRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id)));
        return oUser;
    }

    //Academic
    @Override
    public List<AcademicEntity> findAllAcademic(Integer id){
        List <AcademicEntity> academic=findUserById(id).get().getAcademicList().stream().toList();
        return academic;

    }

    //Projects
    @Override
    public List<ProjectEntity> findAllProject(Integer id){
        List<ProjectEntity> projectList=findUserById(id).get().getProjectList().stream().toList();
        return projectList;

    }

}
