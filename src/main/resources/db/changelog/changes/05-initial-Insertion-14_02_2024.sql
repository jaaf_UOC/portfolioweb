--liquibase formatted sql
--changeset joseA:01-initial-insertion-14_02_2024_16:00

INSERT INTO user_entity
(
  id,
  name,
  surname,
  iden_Document,
  user_Email
)

VALUES
(1,'Jose Antonio', 'Artero', '0000001A', 'jaaf_2@hotmail.com');

INSERT INTO project_entity
(
    project_Id,
    project_Name,
    project_Description,
    user_Id
)
VALUES
(1, 'Portofolio personal','Creación de una plataforma personal', 1);

INSERT INTO academic
(
    academic_Id,
    title_name,
    start_date,
    end_date,
    description,
    user_Id
)

VALUES
(1, 'Grado en Ingeniería Informática', '10/09/2017', '01/12/2024', 'Grado en ingeniería del software en la UOC', 1);

INSERT INTO work_Experience
(
    work_Id,
    company_name,
    job,
    start_Job_Date,
    end_Job_Date,
    job_Description,
    user_Id
)

VALUES
(1, 'Grupo Pôsit', 'Mantenimiento de sistemas', '01/06/2002', '01/09/2010', 'Mantenimineto de sistemas informaticos', 1);