# PortfolioWeb



## En desarrollo

El actual proyecto trata de poner en práctica conceptos vistos en el grado, partiendo de unos requisitos <br>
mínimos para poder enfocarse en la utilización de herraminetyas y lenguajes y no tanto en funcionalidades<br>
extensas.

Algunos de los conceptos a desarrollar son:<br>
[] Servicios con Spring Boot<br>
[] Inclusión de Swagger <br>
[] Liquibase<br>
[] Pruebas con JUnit<br>
[] Spring Security<br>





